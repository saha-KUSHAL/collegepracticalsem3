//import java.util.concurrent.Semaphore;

class ProducerConsumer {
  static int count;

  // static Semaphore full = new Semaphore(0);
  // static Semaphore empty = new Semaphore(2);
  // static Semaphore mutex = new Semaphore(1);
  static class Producer implements Runnable {
    public void run() {
      while (true) {
        try {
          // empty.acquire();
          // mutex.acquire();
          if (count < 3) {
            System.out.println("The producer is produceing");
            count++;
          } else {
            System.out.println("The producer is sleeping !");
          }
          // mutex.release();
          // full.release();
          Thread.sleep(1000);
        } catch (Exception e) {
        }
      }
    }
  }

  static class Consumer implements Runnable {
    public void run() {
      while (true) {
        try {
          // full.acquire();
          // mutex.acquire();
          if (count > 0) {
            System.out.println("Consumer is  consumeing.");
            count--;
          } else
            System.out.println("Consumer is sleeping !");
          // mutex.release();
          // empty.release();
          Thread.sleep(1500);
        } catch (Exception c) {
        }
      }
    }
  }

  public static void main(String[] args) {

    Producer p = new Producer();
    Consumer c = new Consumer();

    Thread t1 = new Thread(p);
    Thread t2 = new Thread(c);

    t1.start();
    t2.start();
    try {
      t2.join();
      t1.join();
    } catch (Exception r) {
    }
  }
}
