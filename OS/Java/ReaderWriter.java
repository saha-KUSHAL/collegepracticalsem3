import java.util.concurrent.Semaphore;

class ReaderWriter {

	static Semaphore wrt = new Semaphore(1);
	static Semaphore mutex = new Semaphore(1);
	static int readCount = 0;

	static class Reader implements Runnable {

		public void run() {
			try {
				mutex.acquire();
				readCount++;
				if (readCount == 1) // First reader
					wrt.acquire();
				mutex.release();
				System.out.println(Thread.currentThread().getName() + " is reading");
				Thread.sleep(1500);
				System.out.println(Thread.currentThread().getName() + " finished reading.");
				mutex.acquire();
				readCount--;
				if (readCount == 0) // Last Thread
					wrt.release();
				mutex.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	static class Writer implements Runnable {
		public void run() {
			try {
				wrt.acquire();
				System.out.println(Thread.currentThread().getName() + " is writing.");
				Thread.sleep(1500);
				System.out.println(Thread.currentThread().getName() + " finished writing.");
				wrt.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		Reader reader = new Reader();
		Writer writer = new Writer();
		Thread r1 = new Thread(reader);
		r1.setName("Reader 1");
		Thread r2 = new Thread(reader);
		r2.setName("Reader 2");
		Thread r3 = new Thread(reader);
		r3.setName("Redaer 3");
		Thread w1 = new Thread(writer);
		w1.setName("Writer 1");
		Thread w2 = new Thread(writer);
		w2.setName("Writer 2");
		r1.start();
		r2.start();
		w1.start();
		w2.start();
		r3.start();
	}
}