import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;

public class Rsa {
    private int p, q;
    private BigInteger e, d, n, phie;

    public Rsa() {
        p = generatePrime(128);
        do {
            q =generatePrime(128);
        } while (p == q);
        n = BigInteger.valueOf((long) p * q);
        
        phie = BigInteger.valueOf((long) (p - 1) * (q - 1));
        generatePublicKey();
        System.out.println("The public key is " + e);
        generatePrivateKey();
        System.out.println("The private key is " + d);
    }

    private int generatePrime(int bound) {
        Random random = new Random();
        int n = random.nextInt(bound);
        for (int i = 0; !isPrime(n); i++)
            n = random.nextInt(bound);
        return n;
    }

    private boolean isPrime(int num) {
        if (num < 2)
            return false;
        for (int i = 2; i < num / 2; i++)
            if (num % i == 0)
                return false;
        return true;
    }

    private int gcd(int a, int b) {
        while (b != 0) {
            int rem = a % b;
            a = b;
            b = rem;
        }
        return a;
    }

    private void generatePublicKey() {
        for (int i = 2; i < phie.intValue(); i++) {
            if (gcd(i, phie.intValue()) == 1) {
                e = BigInteger.valueOf(i);
                break;
            }
        }
    }

    private void generatePrivateKey() {
        d = (e.add(BigInteger.valueOf(1)));
        BigInteger temp = d.multiply(e);
        while (!(temp.remainder(phie)).equals(BigInteger.valueOf(1))) {
            d = d.add(BigInteger.valueOf(1));
            temp = d.multiply(e);
        }
    }

    public BigInteger encrypt(BigInteger m) {
        BigInteger cypher;
        cypher = (m.pow(e.intValue())).mod(n);
        return cypher;
    }

    private BigInteger decrypt(BigInteger cypher) {
        BigInteger m;
        m = (cypher.pow(d.intValue()).mod(n));
        return m;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your message :");
        BigInteger message = scanner.nextBigInteger();
        Rsa rsa = new Rsa();
        BigInteger c = rsa.encrypt(message);
        System.out.println("The encrypted message : " + c);
        System.out.println("The Decrypted message :" + rsa.decrypt(c));
    }
}
