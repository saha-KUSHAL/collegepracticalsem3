#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
typedef struct node
{
    int at, bt, ct, rt, wt, tat;
    bool com;
} process;

int main()
{
    process *p;
    int n, i, time = 0, completed = 0, total_wt = 0, time_slice = 4;
    printf("Enter the no of process:");
    scanf("%d", &n);
    printf("Enter the amount of time slice:");
    scanf("%d", &time_slice);
    p = (process *)calloc(n, sizeof(process));
    if (p == NULL)
    {
        printf("Can not allocate memeory !");
        return 1;
    }
    for (i = 0; i < n; i++)
    {
        printf("Enter the arival time for process %d: ", i + 1);
        scanf("%d", &p[i].at);
        printf("Enter the burst time for process %d: ", i + 1);
        scanf("%d", &p[i].bt);
        p[i].rt = p[i].bt;
        p[i].com = false;
    }

    for (i = 0; completed < n; i++)
    {
        int index = -1, j;
        for (j = 0; j < n; j++)
        {
            if (p[j].at <= time && p[j].com == false)
            {
                index = (index + 1) % n;
                break;
            }
        }
        if (index == -1)
            time++;
        else
        {
            if (p[index].rt <= time_slice)
            {
                time += p[index].rt;
                p[index].rt = 0;
                p[index].com = true;
            }
            else
            {
                time += time_slice;
                p[index].rt = p[index].rt - time_slice;
            }
        }
        if (p[index].com)
        {
            p[index].ct = time;
            p[index].tat = p[index].ct - p[index].at;
            p[index].wt = p[index].tat - p[index].bt;
            total_wt += p[index].wt;
            completed++;
        }
    }
    printf(
        "PID\tArival Time\tBurst Time\tCompletion Time\tWaiting Time\tT. Around "
        "Time\n");
    for (i = 0; i < n; i++)
    {
        printf("%d\t%d\t\t%d\t\t%d\t\t%d\t\t%d\n", i + 1, p[i].at, p[i].bt, p[i].ct,
               p[i].wt, p[i].tat);
    }
    printf("\nAverage waiting time: %d\n", total_wt / n);
}