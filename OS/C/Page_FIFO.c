// 

#include <stdio.h>
#define MAX 100
int page[MAX], page_size;
int frame[MAX], frame_size;
int isAvailable(int);

int main()
{
    int i, page_fault = 0, frame_index = 0, k;
    char hm = 'H';
    printf("Enter the number of pages: ");
    scanf("%d", &page_size);
    printf("Enter pages: ");
    for (i = 0; i < page_size; i++)
        scanf("%d", &page[i]);
    printf("Enter the frame size:");
    scanf("%d", &frame_size);
    printf("\nString\tHit/Miss\t");
    for (i = 0; i < frame_size; i++)
    {
        printf("Frame %d\t", i + 1);
        frame[i] = -1;
    }
    printf("\n");
    for (i = 0; i < page_size; i++, hm = 'H')
    {
        if (!isAvailable(page[i]))
        {
            hm = 'M';
            page_fault++;
            if (i < frame_size)
            {
                frame[i] = page[i];
            }
            else
            {
                frame[(frame_index) % frame_size] = page[i];
                frame_index++;
            }
        }
        printf("%d", page[i]);
        printf("\t%c\t", hm);
        for (k = 0; k < frame_size; k++)
            printf("\t%d", frame[k]);
        printf("\n");
    }
    printf("\nPagefault : %d\n", page_fault);
    return 0;
}

int isAvailable(int key)
{
    int i;
    for (i = 0; i < frame_size; i++)
        if (frame[i] == key)
            return 1;
    return 0;
}