#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
typedef struct node
{
  int at, bt, ct, rt, wt, tat;
  bool com;
} process;

int main()
{
  process *p;
  int n, i, time = 0, completed = 0, total_wt = 0;
  printf("Enter the no of process:");
  scanf("%d", &n);
  p = (process *)calloc(n, sizeof(process));
  if (p == NULL)
  {
    printf("Can not allocate memeory !");
    return 1;
  }
  for (i = 0; i < n; i++)
  {
    printf("Enter the arival time for process %d: ", i + 1);
    scanf("%d", &p[i].at);
    printf("Enter the burst time for process %d: ", i + 1);
    scanf("%d", &p[i].bt);
    p[i].rt = p[i].bt;
    p[i].com = false;
  }

  while (completed < n)
  {
    int min_index = -1;
    int min_at = 999;
    for (i = 0; i < n; i++)
    {
      if (p[i].at <= time && p[i].com != true && p[i].at < min_at)
      {
        min_index = i;
        min_at = p[i].at;
      }
    }
    if (min_index != -1)
    {
      time += p[min_index].bt;
      p[min_index].com = true;
      p[min_index].ct = time;
      p[min_index].tat = p[min_index].ct - p[min_index].at;
      p[min_index].wt = p[min_index].tat - p[min_index].bt;
      total_wt += p[min_index].wt;
      completed++;
    }
    else
      time++;
  }
  printf(
      "PID\tArival Time\tBurst Time\tCompletion Time\tWaiting Time\tT. Around "
      "Time\n");
  for (i = 0; i < n; i++)
  {
    printf("%d\t%d\t\t%d\t\t%d\t\t%d\t\t%d\n", i + 1, p[i].at, p[i].bt, p[i].ct,
           p[i].wt, p[i].tat);
  }
  printf("\nAverage waiting time: %f\n", (float)total_wt / n);
}