#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
typedef struct node
{
    int at, bt, ct, wt, rt, tat, priority;
    bool com;
} process;

int main()
{
    int n, i, completed = 0, time = 0, total_wt = 0;
    process *p;
    printf("Enter the number of process:");
    scanf("%d", &n);
    p = (process *)calloc(n, sizeof(process));
    if (p == NULL)
    {
        printf("Cannot allocate memory.");
        return 1;
    }
    for (i = 0; i < n; i++)
    {
        printf("Enter the priority of process %d:", i + 1);
        scanf("%d", &p[i].priority);
        printf("Enter the arival time of process %d:", i + 1);
        scanf("%d", &p[i].at);
        printf("Enter the burst time of process %d:", i + 1);
        scanf("%d", &p[i].bt);
        p[i].com = false;
    }

    while (completed < n)
    {
        int highest = 999;
        int highest_index = -1;
        for (i = 0; i < n; i++)
        {
            if (p[i].at <= time && p[i].com == false && p[i].priority < highest)
            {
                highest = p[i].priority;
                highest_index = i;
            }
        }
        if (highest_index == -1)
            time++;
        else
        {
            time += p[highest_index].bt;
            p[highest_index].ct = time;
            p[highest_index].tat = p[highest_index].ct - p[highest_index].at;
            p[highest_index].wt = p[highest_index].tat - p[highest_index].bt;
            p[highest_index].com = true;
            completed++;
            total_wt += p[highest_index].wt;
        }
    }

    printf(
        "PID  Priority  Arival Time  Burst Time  Completion Time  Waiting Time  T. Around "
        "Time\n");
        
    for (i = 0; i < n; i++)
    {
        printf("%d    %d         %d            %d           %d                %d             %d\n", i + 1, p[i].priority, p[i].at, p[i].bt, p[i].ct,
               p[i].wt, p[i].tat);
    }
    printf("\nAverage waiting time: %f\n",(float)total_wt / n);
    return 0;
}