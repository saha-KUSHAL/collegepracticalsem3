#include<stdio.h>
#include<stdlib.h>
int bufferCount=0,bufferSize=2,procall=0,concall=0;
void producer();
void consumer();

int main(){
  int ch;
  do{
    printf("\n\n-------- Bounded Buffer -------");
    printf("\n1: Producer\t2: Consumer");
    printf("\nEnter> ");
    scanf("%d",&ch);
    switch(ch){
      case 1: producer();
      break;
      case 2: consumer();
      break;
      case 0: exit(0);
    }
  }while(1);
  return 0;
}

void producer(){
  if(bufferCount < bufferSize){
    if(bufferCount == 0 && concall)
      printf("\nProducer waked up");
    printf("\nProducer produced an item.");
    bufferCount++;
    procall=0;
  }
  else{
  printf("\nBuffer full ! Producer is sleeping.");
  procall=1;
  }
}

void consumer(){
  if(bufferCount > 0)
  {
    if(bufferCount == bufferSize && procall)
      printf("\nConsumer Waked up.");
    printf("\nConsumer consumed an item.");
    bufferCount--;
    concall=0;
  }
  else{
    printf("\nBuffer is empty ! Consumer is sleeping.");
    concall=1;
  }

}
