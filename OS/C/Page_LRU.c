#include <stdio.h>
int frame[10], frameCount, page[100], pageCount;
int isAvailable(int);
int find_least(int);
int main()
{
    int i, pageFault = 0, frameIndex = 0, k;
    char hm = 'H';
    printf("Enter the no of pages:");
    scanf("%d", &pageCount);
    printf("Enter the pages:\n");
    for (i = 0; i < pageCount; i++)
        scanf("%d", &page[i]);
    printf("Enter the no of frames:");
    scanf("%d", &frameCount);
    printf("String\tHit/Miss\t");
    for (i = 0; i < frameCount; i++)
    {
        printf("Frame %d\t", i + 1);
        frame[i] = -1;
    }
    printf("\n");
    for (i = 0; i < pageCount; hm = 'H', i++)
    {
        if (!isAvailable(page[i]))
        {   
            hm = 'M';
            pageFault = pageFault + 1;
            if (frameIndex < frameCount)
                frame[frameIndex++] = page[i];
            else
            {
                int max = find_least(i);
                frame[max] = page[i];
            }
        }
        printf("%d", page[i]);
        printf("\t%c\t", hm);
        for (k = 0; k < frameCount; k++)
            printf("\t%d", frame[k]);
        printf("\n");
    }

    printf("\nPagefault : %d", pageFault);
}

int isAvailable(int key)
{
    int i;
    for (i = 0; i < frameCount; i++)
    {
        if (frame[i] == key)
            return 1;
    }
    return 0;
}

int find_least(int currentIndex)
{
    int i, j, least_index,least_pos=999;
    for (i = 0; i < frameCount; i++)
    {
        for (j = currentIndex - 1; j >= 0; j--)
        {
            if (page[j] == frame[i])
            {
                if (j < least_pos)
                {
                    least_pos = j;
                    least_index = i;
                }
                break;
            }
        }
    }
    return least_index;
}