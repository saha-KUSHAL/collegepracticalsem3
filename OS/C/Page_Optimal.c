#include <stdio.h>
int frame[10], frameCount, page[100], pageCount;
int isAvailable(int);
int findMax(int);
int main()
{
  int i, pageFault = 0, frameIndex = 0, k;
  char hm = 'H';
  printf("Enter the no of pages:");
  scanf("%d", &pageCount);
  printf("Enter the pages:\n");
  for (i = 0; i < pageCount; i++)
    scanf("%d", &page[i]);
  printf("Enter the no of frames:");
  scanf("%d", &frameCount);
  printf("String\tHit/Miss\t");
  for (i = 0; i < frameCount; i++)
  {
    printf("Frame %d\t", i + 1);
    frame[i] = -1;
  }
  printf("\n");
  for (i = 0; i < pageCount; hm = 'H', i++)
  {
    if (!isAvailable(page[i]))
    {
      hm = 'M';
      pageFault = pageFault + 1;
      if (frameIndex < frameCount)
        frame[frameIndex++] = page[i];
      else
      {
        int max = findMax(i);
        frame[max] = page[i];
      }
    }
    printf("%d", page[i]);
    printf("\t%c\t", hm);
    for (k = 0; k < frameCount; k++)
      printf("\t%d", frame[k]);
    printf("\n");
  }

  printf("\nPagefault : %d", pageFault);
}

int isAvailable(int key)
{
  int i;
  for (i = 0; i < frameCount; i++)
  {
    if (frame[i] == key)
      return 1;
  }
  return 0;
}

int findMax(int currentPageIndex)
{
  int max = 0, i, j, pos[10];
  for (i = 0; i < frameCount; i++)
  {
    for (j = currentPageIndex; j < pageCount; j++)
    {
      if (page[j] == frame[i])
      {
        pos[i] = j;
        break;
      }
      else
        pos[i] = pageCount + 1;
    }
  }

  for (i = 0, max = 0; i < frameCount; i++)
  {
    if (pos[max] > pos[i])
      max = i;
  }
  return max;
}
