# Write a Shell program to check and display 10 leap years
leapYear()
{
    if test $(( $1 % 400 )) -eq 0
    then
        return 0
    fi
    if test $(( $1 % 100 )) -ne 0 && test $(( $1 % 4 )) -eq 0
    then
        return 0
    else
        return 1
    fi
}
echo "--- Program to calculate previous 10 leap year--"
echo -n "Enter current year: "
read year

count=0

while test $count -le 10
do
    if leapYear $year 
    then
        echo "$year"
        count=$(( $count + 1 ))
    fi
    year=$(( $year - 1 ))
done