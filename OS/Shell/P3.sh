# Write a Shell program to generate prime numbers between 1 and 50.

is_prime(){
    num=$1

    if [ $num -lt 2 ]; then
        return 1
    fi

    for(( k=2; k <= num/2; k++)); do
        if [ $(( $num % $k)) -eq 0 ]; then
            return 1
        fi
    done

    return 0
}
i=1
while test $i -le 50
do
    if is_prime $i 
    then 
        echo $i
    fi
    i=$(( $i + 1 ))
done