fact()
{
    i=$1
    res=1
    while test $i -gt 0
    do
        res=$(( res * $i ))
        i=$(( $i - 1 ))
    done
    return $res
}

echo -n "Enter an integer number to calculate it's factorial : "
read num
fact $num

echo "Factorial of $num is $?"