while true
do
    echo -e "\n1. Display Directory contents"
    echo "2. Display Directrory contents with more details"
    echo "3. Display current directory path"
    echo "4. Clear the screen"
    echo "0. Exit"
    echo -n "Enter : "
    read n
    case $n in
        1 ) ls ;;
        2 ) ls -al ;;
        3 ) pwd ;;
        4 ) clear ;;
        0 ) exit ;;
        * ) echo "Invalid option"
    esac
done