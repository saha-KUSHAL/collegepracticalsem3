echo -n "Enter a number: "
read num
small=$num
while test $num -gt 0
do
    rem=$(( $num % 10 ))
    if test $rem -lt $small
    then
        small=$rem
    fi
    num=$(( num / 10 ))
done

echo "The smallest digit is $small"