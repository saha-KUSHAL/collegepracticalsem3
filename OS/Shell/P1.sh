#   Write a Shell program to check the given number is even or odd. 

is_odd_even(){
    num=$1

    if [ $(($num%2)) -eq 0 ]; then
        echo "$num is a even number"
    else
        echo "$num is a odd number"
    fi
}

echo "Enter a number: "
read n
is_odd_even $n