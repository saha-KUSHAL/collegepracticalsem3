echo "Enter a string: "
read str

vowel=0
i=0
while test $i -lt ${#str}
do
  char="${str:i:1}"
  if [ "$char" = 'a' -o $char = 'e' -o $char = 'i' -o $char = 'o' -o $char = 'u' ]
  then
    vowel=$(( $vowel + 1 ))
  fi
  i=$(( $i + 1 ))
done

echo "The no of vowel is $vowel"  
