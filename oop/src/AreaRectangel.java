import java.util.Scanner;
public class AreaRectangel {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int w,h;
        System.out.println("Enter the width of the rectangle: ");
        w=scanner.nextInt();
        System.out.println("Enter the hight of the rectangle: ");
        h=scanner.nextInt();
        Rectangle rectangle = new Rectangle(w, h);
        System.out.println("The area of the rectangle is "+ rectangle.getArea());
    }
    
}
class Shape 
{
    int getArea(){
        return 0;
    }
}

class Rectangle extends Shape{
    private int width,height;
    Rectangle(int w,int h)
    {
        width = w;
        height = h;
    }
    int getArea(){
        return width * height;
    }
}
