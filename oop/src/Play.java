interface Playable{
    public void play();
}

class Football implements Playable{
    public void play(){
        System.out.println("Playing football");
    }
}
class Volleyball implements Playable{
    public void play(){
        System.out.println("Playing Volleyball");
    }
}
class Basketball implements Playable{
    public void play(){
        System.out.println("Playing Basketball");
    }
}
public class Play{
    public static void main(String[] args) {
        Football football = new Football();
        Volleyball volleyball = new Volleyball();
        Basketball basketball = new Basketball();
        football.play();
        volleyball.play();
        basketball.play();
    }
}