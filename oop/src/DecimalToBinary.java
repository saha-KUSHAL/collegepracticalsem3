import java.util.Scanner;

//Decimal to binary

public class DecimalToBinary {

	public static void main(String[] args) {
		int num = 10;
		int binaryNum = 0;
		int pow = 1;
		int zero = 0;
		int rem;
		while( num > 0) {
			rem = num % 2;
			if(rem == 0)
				zero++;
			binaryNum += rem * pow;
			num = num / 2;
			pow *= 10;
		}
		
		System.out.println(binaryNum);
		System.out.println("The no of 0 digits is: " + zero);
	}
}
