class Person{
    private String name,country;
    private int age;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
}

class PersonClass{
    public static void main(String[] args) {
        Person person = new Person();
        person.setAge(20);
        person.setName("Kushal Saha");
        person.setCountry("India");
        System.out.println("Details: \n" +"Name: " + person.getName()+ " Age : " + person.getAge() + " Country: " + person.getCountry());
    }
}