class BankAccount {
    protected int balance;

    public BankAccount(int initBalance){
        balance = initBalance;
    }

    public void deposite(int amount){
        balance += amount;
    }

    public void withdraw(int amount){
        balance -= amount;
    }
    public int getBalance(){
        return balance;
    }
}

class SavingAccount extends BankAccount{

    public SavingAccount(int initBalance){
        super(initBalance);
    }

    public void withdraw(int amount){
        if(balance < 100)
            System.out.println("Balance is bellow than withdraw limit");
        else
            balance -= amount;
    }
}

public class BankAccountSystem{

    public static void main(String[] args) {
        SavingAccount sAccount = new SavingAccount(100);
        sAccount.withdraw(200);
        System.out.println("The balance is " + sAccount.getBalance());
        sAccount.deposite(600);
        System.out.println("The balance is " + sAccount.getBalance());
        sAccount.withdraw(400);
        System.out.println("The balance is " + sAccount.getBalance());
    }
}