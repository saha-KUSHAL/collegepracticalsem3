import java.util.concurrent.locks.*;
class ReadWriteLockPrac
 {
    private static ReadWriteLock lock = new ReentrantReadWriteLock();
    //Lock readlock = lock.readLock();
    static class Reader implements Runnable{
        public void run(){
            lock.readLock().lock();
            System.out.println( Thread.currentThread().getName() + " Reading data");
            try{
                Thread.sleep(500);
            }catch(Exception e){}
            System.out.println(Thread.currentThread().getName() + " Reading finished");
            lock.readLock().unlock();
        }
    }

    static class Writer implements Runnable{
        public void run(){
            lock.writeLock().lock();
            System.out.println(Thread.currentThread().getName() + " Writing data !");
            try{
                Thread.sleep(1000);
            }catch(Exception e){}
            System.out.println(Thread.currentThread().getName() + " Writing finished");
            lock.writeLock().unlock();
        }
    }

    public static void main(String[] args) {
        Reader reader = new Reader();
        Writer writer =  new Writer();
        Thread t1 = new Thread(reader);
        Thread t2 = new Thread(reader);
        Thread t3 = new Thread(writer);
        Thread t4 = new Thread(writer);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}

