import java.util.Scanner;
public class CountOccurence {

    private int countOccurence(int arr[],int key,int index){
        if(index < 0)
            return 0;
        if (arr[index] == key)
            return 1 + countOccurence(arr, key, index - 1);
        else
            return countOccurence(arr, key, index -1);
    }

    public static void main(String[] args) {
        int arr[];
        Scanner scanner = new Scanner(System.in);
        CountOccurence oc = new CountOccurence();
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();
        arr = new int[size];
        System.out.println("Enter elements: ");
        for(int i=0;i<size;i++)
            arr[i] = scanner.nextInt();
        System.out.print("Enter the element you want to check the occurence: ");
        int key = scanner.nextInt();
        int count =oc.countOccurence(arr, key, arr.length - 1);
        System.out.println("Occurrence of " + key + " is " + count);
    }
}
