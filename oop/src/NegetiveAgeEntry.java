import java.util.Scanner;

class NegetiveAgeEntry {
    // Execption class
    static class NegetiveAgeException extends Exception {
        public NegetiveAgeException() {
            super("Age can't be negetive");
        }
    }

    public static void main(String[] args) throws NegetiveAgeException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the age: ");
        short age = scanner.nextShort();

        // Handeling the exception
        if (age < 0) {
            throw new NegetiveAgeException();
        } else
            System.out.println("The age is " + age);
    }
}