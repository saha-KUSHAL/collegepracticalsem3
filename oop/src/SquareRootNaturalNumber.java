import java.util.concurrent.Semaphore;
public class SquareRootNaturalNumber {

    static Semaphore mutex = new Semaphore(1);
    static double number = 0;
    public static void main(String[] args) {

        GenerateSquareRoot1 gsr1 = new GenerateSquareRoot1();
        GenerateSquareRoot2 gsr2 = new GenerateSquareRoot2();

        Thread t1 = new Thread(gsr1);
        Thread t2 = new Thread(gsr2);

        t1.start();
        t2.start();
    }

    static class GenerateSquareRoot1 implements Runnable {

        public void run() {
            while (number < 30) {
                try {
                    mutex.acquire();
                    System.out.println(Thread.currentThread().getName());
                    System.out.println("The square root of " + number + " is " + Math.sqrt(number));
                    number++;
                    mutex.release();
                    Thread.sleep(500);
                }catch(Exception e){};
            }
        }
    }

    static class GenerateSquareRoot2 implements Runnable {

        public void run() {
            while (number < 30) {
                try {
                    mutex.acquire();
                    System.out.println(Thread.currentThread().getName());
                    System.out.println("The square root of " + number + " is " + Math.sqrt(number));
                    number++;
                    mutex.release();
                    Thread.sleep(500);
                }catch(Exception e){};
            }
        }
    }
}