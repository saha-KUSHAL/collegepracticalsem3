class Shape {
    protected final float pi = 3.14f;
    protected int width, height;

    public Shape(int w, int h) {
        width = w;
        height = h;
    }

    public float getPerimeter() {
        return 0f;
    }

    public float getArea() {
        return 0f;
    }
}

class Circle extends Shape {

    public Circle(int r) {
        super(2 * r, 2 * r);
    }

    public float getPerimeter() {
        return width * pi;
    }

    public float getArea() {
        return pi * (width / 2) * (width / 2);
    }
}

public class ShapeAreaPerimeter {
    public static void main(String[] args) {
        Circle circle = new Circle(5);
        System.out.println("Area:" + circle.getArea());
        System.out.println("Perimeter:" + circle.getPerimeter());
    }
}