
import java.util.Scanner;

public class KSmallestEle {
    public static void main(String[] args) {
        int a[], size;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the no of elements: ");
        size = scanner.nextInt();
        a = new int[size];
        System.out.println("Enter the elements: ");
        for (int i = 0; i < size; i++)
            a[i] = scanner.nextInt();
        sort(a);
        System.out.println("How many smallest element you want :");
        int k = scanner.nextInt();
        scanner.close();
        if (k > size)
            System.out.println("Invalid range");
        else {
            System.out.println(k + " smallest elements of the array are:");
            for (int i = 0; i < k; i++)
                System.err.print(a[i] + "\t");
        }
    }

    static void sort(int a[]) {
        for (int i = 0; i < a.length - 1; i++) {
            for (int j = 0; j < a.length - 1 - i; j++) {
                if (a[j] > a[j + 1]) {
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                }
            }
        }
    }
}