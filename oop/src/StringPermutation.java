import java.util.Scanner;
public class StringPermutation {
    public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);
       System.out.print("Enter the string: ");
       String string = scanner.nextLine();
       int[] visited = new int[string.length()];
       StringBuffer temp = new StringBuffer();
       System.out.println("All string permutations are:");
       permute(string,temp,visited);
    }

    static void permute(String str,StringBuffer temp,int [] visited){
        if(str.length() == temp.length()){
            System.out.println(temp);
            return;
        }
        for(int i = 0; i < visited.length;i++){
            if(visited[i] == 0){
                visited[i]=1;
                temp.append(str.charAt(i));
                permute(str,temp,visited);
                temp.deleteCharAt(temp.length()-1);
                visited[i]=0;
            }
        }
    }
}
