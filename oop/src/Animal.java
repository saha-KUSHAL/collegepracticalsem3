public class Animal {
    public void move(){
        System.out.println("The animal is walking !");
    }
    public void makeSound(){
        System.out.println("The animal is walking !");
    }

    public static void main(String[] args) {
        Bird bird = new Bird();
        Panthera panthera = new Panthera();
        bird.move();
        panthera.move();
        bird.makeSound();
        panthera.makeSound();
    }
}

class Bird extends Animal{
    public void move(){
        System.out.println("Bird moves by flying in the sky ! Sususususuuuu");
    }

    public void makeSound(){
        System.out.println("A bird can make varity of sound. Chip chirrip ");
    }
}

class Panthera extends Animal{
    public void move(){
        System.out.println("Panthera moves in ground ! Tug tug ugg tugg");
    }

    public void makeSound(){
        System.out.println("Panthera sounds : ygaauuuauu gyyagauuuuu");
    }
}