import java.util.Scanner;
interface Sort{
    public void sort(int a[]);
}

class BubbleSort implements Sort{
    @Override
    public void sort(int[] a) {
        for(int i = 0; i<a.length-1;i++){
            System.out.println("Itaration " + (i+1));
            for(int j=0;j<a.length-1-i;j++){
                if(a[j] > a[j+1]){
                    int temp = a[j];
                    a[j] = a[j+1];
                    a[j+1]=temp;
                    Sorting.displayArray(a);
                }
            }
        }
    }
}

class SelectionSort implements Sort{

    @Override
    public void sort(int[] a) {
        for(int i=0;i<a.length-1;i++){
            System.out.println("Itaration " + (i+1));
            int min = i;
            for(int j = i+1;j<a.length;j++){
                if(a[min] > a[j])
                    min = j;
            }
            if(min != i){
                int temp = a[i];
                a[i]=a[min];
                a[min]=temp;
            }
            Sorting.displayArray(a);
        }
    }
    
}
public class Sorting {
    public static void main(String[] args) {
        int a[],b[];
        Scanner scanner = new Scanner(System.in);
        BubbleSort bubbleSort = new BubbleSort();
        SelectionSort selectionSort = new SelectionSort();
        System.out.println("Enter the length of the array: ");
        int length = scanner.nextInt();
        a = new int[length];
        System.out.println("Enter the elements: ");
        for(int i = 0;i<length;i++)
            a[i] = scanner.nextInt();
        scanner.close();
        b=a.clone();
        System.out.println("Before sorting: ");
        displayArray(a);
        System.out.println("\nSorting via bubble sort: ");
        bubbleSort.sort(a);
        System.out.println("After Sorting:");
        displayArray(a);
        System.out.println("\nSorting via selection sort: ");
        selectionSort.sort(b);
        System.out.println("After Sorting:");
        displayArray(b);
    }
    public static void displayArray(int a[]){
        for(int i=0;i<a.length ;i++)
            System.out.print(a[i] + "  ");
        System.out.println();
    }
}
