import java.util.Scanner;
class Employee {
    protected int basic, tax, ha, ta, bonus, overtime;
    protected String role, name;

    public int calculateSalary() {
        return 0;
    }

    public String getName() {
        return name;
    }

    public void setBonus(int b) {
        this.bonus = b;
    }

    public void setOverTime(int m) {
        this.overtime = m;
    }
}

class Manager extends Employee {
    Manager(String name, int basic, int tax, int ha, int ta) {
        this.basic = basic;
        this.name = name;
        this.tax = tax;
        this.ha = ha;
        this.ta = ta;
    }

    public int calculateSalary() {
        return basic - tax + ha + ta + bonus + overtime;
    }
}

class Programmer extends Employee {
    Programmer(String name, int basic, int tax, int ha, int ta) {
        this.basic = basic;
        this.name = name;
        this.tax = tax;
        this.ha = ha;
        this.ta = ta;
    }

    public int calculateSalary() {
        return basic - tax + ha + ta + overtime;
    }
}

public class EmployeeSystem {
    public static void main(String[] args) {
        int basic ;
    Scanner scanner = new Scanner(System.in);
       System.out.println("Enter basic of the manager: ");
       basic = scanner.nextInt(); 
        Employee employee1 = new Manager("Manager", basic, 200, 300, 100);
        employee1.setBonus(500);
System.out.println("Enter basic of the programmer: ");
       basic = scanner.nextInt(); 
        Employee employee2 = new Programmer("Programmer", basic, 100, 300, 50);
        employee2.setOverTime(100);

        System.out.println("Name: " + employee1.getName() + " Salary: " + employee1.calculateSalary());
        System.out.println("Name: " + employee2.getName() + " Salary: " + employee2.calculateSalary());
    }
}
