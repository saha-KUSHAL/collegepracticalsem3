#include<stdio.h>
#define MAX 100
void display();
void insertion_sort();
int size;
int arr[MAX];
int main(){
    int i;
    printf("Enter the size of the array: ");
    scanf("%d",&size);
    printf("Enter the elements: ");
    for(i=0;i<size;i++){
        printf("\nEnter the %d element: ",i+1);
        scanf("%d",&arr[i]);
    }
    printf("\nBefore sorting: ");
    display();
    insertion_sort();
    printf("\nAfter sorting: ");
    display();
    return 0;
}

void insertion_sort()
{
    int key,i,j;
    for(i=1;i<size;i++){
        key = arr[i];
        for(j=i-1; j >= 0 && key < arr[j];j--)
            arr[j+1]=arr[j];
        arr[j+1] = key;
    }
}

void display(){
    int i = 0;
    printf("\n");
    while(i < size)
        printf("%d ",arr[i++]);
    printf("\n");
}