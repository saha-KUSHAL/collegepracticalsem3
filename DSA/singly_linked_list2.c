#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int data;
    struct node *next;
} node;
node *head = NULL;
void create();
void display();
void insert_first(int);
void delete_last();
int main()
{
    int ch;
    do
    {
        if (head == NULL)
            printf("\n1. Create\t");
        else
            printf("\n");
        printf("2:Insert First\t3:Delete Last\t4:Display\t5:Exit");
        printf("\nEnter choice: ");
        scanf("%d",&ch);
        switch (ch)
        {
        case 1:
            create();
            break;
        case 2:
        {
            int data;
            printf("\nEnter the data to be inserted: ");
            scanf("%d",&data);
            insert_first(data);
        }
        break;
        case 3:
            delete_last();
            break;
        case 4:
            display();
            break;
        case 5:
            exit(0);
        }
    } while (ch != 5);
    return 0;
}

void create()
{
    char c;
    node *temp2 = NULL;
    do
    {
        node *new_node = (node *)malloc(sizeof(node));
        printf("\nEnter the data of the node: ");
        scanf("%d", &new_node->data);
        new_node->next = NULL;
        if (head == NULL)
            head = temp2 = new_node;
        else
        {
            temp2->next = new_node;
            temp2 = new_node;
        }
        printf("\nCreate more node ? [y/n] : ");
        scanf(" %c", &c);
    } while (c != 'n');
}

void display()
{
    node *temp2 = head;
    printf("\n");
    if(head == NULL)
        printf("The linked list is empty.");
    while (temp2 != NULL)
    {
        printf("%d ", temp2->data);
        if (temp2->next != NULL)
            printf("-> ");
        temp2 = temp2->next;
    }
    printf("\n");
}

void insert_first(int data)
{
    node *new_node = (node *)malloc(sizeof(node));
    new_node->data = data;
    new_node->next = NULL;
    if (head == NULL)
        head = new_node;
    else
    {
        new_node->next = head;
        head = new_node;
    }
}

void delete_last()
{
    node *temp = head;
    if (temp == NULL)
        printf("\nThe Linked List is empty !.\n");
    else
    {
        if (head->next == NULL)
        {
            printf("\n%d is deleted.\n", head->data);
            free(head);
            head = NULL;
        }
        else
        {
            while (temp->next->next != NULL)
                temp = temp->next;
            printf("\n%d is deleted.\n", temp->next->data);
            free(temp->next);
            temp->next = NULL;
        }
    }
}