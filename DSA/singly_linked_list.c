#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int data;
    struct node *next;
} node;
node *head = NULL;
void create();
void display();
void insert_anywhere(int, int);
int main()
{
    int ch;
    do
    {
        if (head == NULL)
            printf("\n1. Create\t");
        printf("2:Insert Anywhere\t3:Display\t4:Exit");
        printf("\nEnter choice: ");
        scanf(" %d", &ch);
        switch (ch)
        {
        case 1:
            create();
            break;
        case 2:
        {
            int data,pos;
            printf("\nEnter the data to be inserted: ");
            scanf("%d", &data);
            printf("\nEnter the position: ");
            scanf("%d",&pos);
            insert_anywhere(data,pos);
        }
        break;
        case 3:
            display();
            break;
        case 4:
            exit(0);
        }
    } while (ch != 4);
    return 0;
}

void create()
{
    char c;
    node *temp2 = NULL;
    do
    {
        node *new_node = (node *)malloc(sizeof(node));
        printf("\nEnter the data of the node: ");
        scanf("%d", &new_node->data);
        new_node->next = NULL;
        if (head == NULL)
            head = temp2 = new_node;
        else
        {
            temp2->next = new_node;
            temp2 = new_node;
        }
        printf("\nCreate more node ? [y/n] : ");
        scanf(" %c", &c);
    } while (c != 'n');
}

void display()
{
    node *temp2 = head;
    printf("\n");
    if(head == NULL)
        printf("\nThe linked list is empty");
    while (temp2)
    {
        printf("%d ", temp2->data);
        if (temp2->next != NULL)
            printf("-> ");
        temp2 = temp2->next;
    }
    printf("\n");
}

void insert_anywhere(int data,int pos)
{
    node *temp = head, *new_node = NULL;
    if (pos == 1)
    {
        new_node = (node *)malloc(sizeof(node));
        new_node->data = data;
        new_node->next = head;
        head = new_node;
    }
    else
    {
        while (pos > 2 && temp != NULL)
        {
            temp = temp->next;
            pos--;
        }
        if (temp == NULL)
            printf("\nInsertion in this position is not possible.\n");
        else
        {
            new_node = (node *)malloc(sizeof(node));
            new_node->data = data;
            new_node->next = temp->next;
            temp -> next = new_node;
        }
    }
}
