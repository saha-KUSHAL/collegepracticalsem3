#include <stdio.h>
#include <stdlib.h>
#define MAX 100
int arr[MAX];
int size = 0;
void display();
void insert(int, int);
void create();
int main()
{
    int ch;
    do
    {
        printf("\n");
        if (size == 0)
            printf("1.Create\t");
        printf("2:Insert\t3:Display\t0:Exit");
        printf("\nEnter choice: ");
        scanf("%d", &ch);
        switch (ch)
        {
        case 1:
            create();
            break;
        case 2:
        {
            int n, p;
            printf("enter the value to insert:");
            scanf("%d", &n);
            printf("enter the position:");
            scanf("%d", &p);
            insert(n, p - 1);
        }
        break;
        case 3:
            display();
            break;
        case 0:
            exit(0);
        }
    } while (ch != 0);
    return 0;
}

void insert(int n, int pos)
{
    int i;
    if (pos > size)
    {
        printf("Can't insert. Previous position is not intialized.\n");
        return;
    }
    for (i = size; i > pos; i--)
    {
        arr[i] = arr[i - 1];
    }
    arr[pos] = n;
    size++;
}
void display()
{
    int i;
    printf("\n");
    if (size == 0)
    {
        printf("\nArray is empty.");
        return;
    }

    for (i = 0; i < size; i++)
        printf("%d ", arr[i]);
    printf("\n");
}
void create()
{
    int i;
    printf("Enter the no of elements in the array: ");
    scanf("%d", &size);
    for (i = 0; i < size; i++)
    {
        printf("Enter the %d is element: ", i + 1);
        scanf("%d", &arr[i]);
    }
}