#include <stdio.h>
#include <stdlib.h>
#define MAX 100
int q[MAX], size = 3, rear = -1, front = -1;
void enqueue(int);
void dequeue();
void display();
int main()
{
    int c, data;
    do
    {
        printf("\n-------- Circular Queue --------");
        printf("\n1: ENQUEUE\t2: DEQUEUE\t3: DISPLAY\t0: Exit");
        printf("\nEnter : ");
        scanf("%d", &c);
        switch (c)
        {
        case 1:
            printf("\nEnter the data (integer): ");
            scanf("%d", &data);
            enqueue(data);
            break;
        case 2:
            dequeue();
            break;
        case 3:
            display();
            break;
        case 0:
            exit(0);
        }
        printf("\n");
    } while (c != 0);
    return 0;
}

void enqueue(int n)
{
    if (rear == -1)
        rear = front = 0;
    else if ((front == 0 && rear == size - 1) || rear == front - 1)
    {
        printf("\nThe queue is full !");
        return;
    }
    else
        rear = (rear + 1) % size;
    q[rear] = n;
}

void dequeue()
{
    if (front == -1)
        printf("\nThe queue is empty !\n");
    else if (front == rear)
    {
        printf("\n%d is deleted.\n", q[front]);
        front = rear = -1;
    }
    else
        printf("\n%d is deleted.\n", q[front++]);
}

void display()
{
    int i;
    printf("\nQueue status\n------------\n");
    if (front <= rear)
    {
        for (i = front; i <= rear; i++)
            printf("%d ", q[i]);
    }
    else
    {
        for (i = front; i < size; i++)
            printf("%d ", q[i]);
        for (i = 0; i <= rear; i++)
            printf("%d ", q[i]);
    }
    printf("\n");
}