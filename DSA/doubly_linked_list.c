#include <stdio.h>
#include <stdlib.h>
// create display delete_anywhere
typedef struct node
{
    struct node *prev;
    int data;
    struct node *next;
} node;

node *head = NULL;

void create();
void delete_anywhere();
void display();

int main()
{
    int ch;
    do
    {
        if (head == NULL)
            printf("\n1. Create\t");
        printf("2:Delete\t3:Display\t4:Exit");
        printf("\nEnter choice: ");
        scanf("%d", &ch);
        switch (ch)
        {
        case 1:
            create();
            break;
        case 2:
            delete_anywhere();
            break;
        case 3:
            display();
            break;
        case 4:
            exit(0);
        }
    } while (ch != 4);
    return 0;
}
void create()
{
    char c = '\0';
    node *temp = NULL;
    while (c != 'n')
    {
        node *new_node = (node *)malloc(sizeof(new_node));
        printf("\nEnter the data: ");
        scanf("%d", &new_node->data);
        new_node->prev = NULL;
        new_node->next = NULL;

        if (head == NULL)
            head = temp = new_node;
        else
        {
            temp->next = new_node;
            new_node->prev = temp;
            temp = temp->next;
        }

        printf("\nDo you want to more node ? [y/n]: ");
        scanf(" %c", &c);
    }
}

void display()
{
    node *temp = head;
    printf("\n");
    if (head == NULL)
    {
        printf("\nThe linked list empty.\n");
        return;
    }
    while (temp != NULL)
    {
        printf("%d ", temp->data);
        if (temp->next != NULL)
            printf("-> ");
        temp = temp->next;
    }
    printf("\n");
}
void insert_at_end(int data)
{
    node *new_node = (node *)malloc(sizeof(node));
    new_node->data = data;
    new_node->next = NULL;
    new_node->prev = NULL;
    if (head == NULL)
        head = new_node;
    else
    {
        node *temp = head;
        while (temp->next != NULL)
            temp = temp->next;
        temp->next = new_node;
        new_node->prev = temp;
    }
}

void insert_at_begining(int data)
{
    node *new_node = (node *)malloc(sizeof(node));
    new_node->data = data;
    new_node->prev = NULL;
    new_node->next = head;
    head->prev = new_node;
    head = new_node;
}

void delete_anywhere()
{
    node *temp = head;
    int data, flag = 0;
    printf("Enter the data to be deleted: ");
    scanf("%d", &data);
    if (head == NULL)
    {
        printf("\nThe list is empty \n");
        return;
    }
    if (head->data == data)
    {
        printf("\n%d is deleted.\n", head->data);
        free(head);
        head->prev = NULL;
        head->next = NULL;
        head=NULL;
        return;
    }
    while (temp != NULL)
    {
        if (temp->data == data)
        {
            printf("\n%d is deleted.\n", temp->data);
            temp->prev->next = temp->next;
            if (temp->next != NULL)
                temp->next->prev = temp->prev;
            free(temp);
            flag = 1;
            break;
        }
        temp = temp->next;
    }
    if (flag == 0)
        printf("\n%d is not found.\n", data);
}