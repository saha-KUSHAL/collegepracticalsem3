#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int coeff;
    int expo;
    struct node *next;
} term;
void add_term(term **, int, int);
void display(term *);
void addition(term *,term *);
term *poly1 = NULL, *poly2 = NULL, *poly3 = NULL;

int main()
{
    int no_of_terms;
    printf("Polynomial 1:\n");
    printf("\nEnter the no of terms:");
    scanf("%d", &no_of_terms);
    while (no_of_terms > 0)
    {
        int coeff, expo;
        printf("\nEnter the co efficient: ");
        scanf("%d", &coeff);
        printf("Enter the exponent: ");
        scanf("%d", &expo);
        add_term(&poly1, coeff, expo);
        no_of_terms--;
    }
    printf("\nPolynomial 2:\n");
    printf("\nEnter the no of terms:");
    scanf("%d", &no_of_terms);
    while (no_of_terms > 0)
    {
        int coeff, expo;
        printf("\nEnter the co efficient: ");
        scanf("%d", &coeff);
        printf("Enter the exponent: ");
        scanf("%d", &expo);
        add_term(&poly2, coeff, expo);
        no_of_terms--;
    }
    printf("\nThe 1st Polynomial is :");
    display(poly1);
    printf("\nThe 2st Polynomial is :");
    display(poly2);
    addition(poly1,poly2);
    printf("\nThe addition of two polynomial is");
    display(poly3);
    return 0;
}

void add_term(term **poly, int coeff, int expo)
{
    term *temp = *poly, *new_term = (term *)malloc(sizeof(term));
    new_term->coeff = coeff;
    new_term->expo = expo;
    new_term->next = NULL;
    if (temp == NULL)
        *poly = new_term;
    else
    {
        while (temp->next != NULL)
            temp = temp->next;
        temp->next = new_term;
    }
}

void display(term *temp)
{
    printf("\n");
    while (temp != 0)
    {
        printf("%dx^%d", temp->coeff, temp->expo);
        if (temp->next != NULL)
            printf(" + ");
        temp = temp->next;
    }
    printf("\n");
}

void addition(term *p1, term *p2)
{
    while (p1 != NULL && p2 != NULL)
    {
        if (p1->expo == p2->expo)
        {
            add_term(&poly3, p1->coeff + p2->coeff, p1->expo);
            p1 = p1->next, p2 = p2->next;
        }
        else if (p1->expo > p2->expo)
        {
            add_term(&poly3, p1->coeff, p1->expo);
            p1 = p1->next;
        }
        else
        {
            add_term(&poly3, p2->coeff, p2->expo);
            p2 = p2->next;
        }
    }
    while (p1 != NULL)
    {
        add_term(&poly3, p1->coeff, p1->expo);
        p1 = p1->next;
    }
    while (p2 != NULL)
    {
        add_term(&poly3, p2->coeff, p2->expo);
        p2 = p2->next;
    }
}