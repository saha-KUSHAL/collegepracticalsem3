#include <stdio.h>
#define MAX 100

int arr[MAX][MAX], k = 0;
int sparse_mat[3][MAX];
void display(int, int);
void take_input(int, int);
int is_sparse(int, int);
void displaySparseMat();
int main()
{
    int row, col, i;
    printf("Enter the row size:");
    scanf("%d", &row);
    printf("\nEnter the column size:");
    scanf("%d", &col);
    printf("Enter the elements:");
    take_input(row, col);
    printf("The matrix is :\n");
    display(row, col);

    if (is_sparse(row, col))
    {
        printf("\nThe matrix is a sparse matrix.");
        displaySparseMat();
    }
    else
        printf("\nThe matrix is not a sparse matrix.");
    return 0;
}

void take_input(int col, int row)
{
    int i, j;
    for (i = 0; i < row; i++)
    {
        for (j = 0; j < col; j++)
            scanf("%d", &arr[i][j]);
    }
}
void display(int row, int col)
{
    int i, j;
    printf("\n");
    for (i = 0; i < row; i++)
    {
        for (j = 0; j < col; j++)
            printf("%d ", arr[i][j]);
        printf("\n");
    }
}

int is_sparse(int row, int col)
{
    int count = 0, i, j;
    for (i = 0; i < row; i++)
    {
        for (j = 0; j < col; j++)
        {
            if (arr[i][j] == 0)
                count++;
            else
            {
                sparse_mat[0][k] = arr[i][j];
                sparse_mat[1][k] = i;
                sparse_mat[2][k] = j;
                k++;
            }
        }
    }
    if (count > (row * col) / 3)
        return 1;
    else
        return 0;
}

void displaySparseMat()
{
    int i;
    printf("\nData\tI'th index\tJ'th index\n");
    for (i = 0; i < k; i++)
    {
        printf("%d\t%d\t\t%d\n",sparse_mat[0][i],sparse_mat[1][i],sparse_mat[2][i]);
    }
}