#include <stdio.h>
#include <stdlib.h>

#define MAX 3
int q[MAX], front = -1, rear = -1;

void enqueue(int);

void dequeue();

void display();

int main()
{
    int c, data;
    do {
        printf("\n-------- Double Ended Queue --------");
        printf("\n1: ENQUEUE\t2: DEQUEUE\t3: DISPLAY\t0: Exit");
        printf("\nEnter : ");
        scanf("%d", &c);
        switch (c) {
            case 1:
                printf("\nEnter the data (integer): ");
                scanf("%d", &data);
                enqueue(data);
                break;
            case 2:
                dequeue();
                break;
            case 3:
                display();
                break;
            case 0:
                exit(0);
        }
        printf("\n");
    } while (c != 0);
    return 0;
}

void enqueue(int data)
{
    int side;
    printf("\nInsert in which end ? [ 1:Front 2:Rear ] :  ");
    scanf("%d", &side);
    if (rear == -1) {
        rear = front = 0;
        q[rear] = data;
    } else {
        if ((front == 0 && rear == MAX - 1) || front == (rear + 1)) {
            printf("\nThe queue is full !");
            return;
        }
        if (side == 2) {
            rear = (rear + 1) % MAX;
            q[rear] = data;
        } else if (side == 1) {
            if (front >= 0) {
                for (int i = ++rear; i > front; i--)
                    q[i] = q[i - 1];
                q[front] = data;
            } else
                q[--front] = data;
        }
    }
}

void display()
{
    int i;
    printf("\nQueue status\n------------\n");
    if(rear == -1 ){
        printf("\nThe queue is empty.\n");
        return;
    }
    if (front <= rear) {
            if(rear == -1 ){
                        printf("\nThe queue is empty.\n");
                                return;
                                    }
                if(rear == -1 ){
                            printf("\nThe queue is empty.\n");
                                    return;
                                        }
                    if(rear == -1 ){
                                printf("\nThe queue is empty.\n");
                                        return;
                                            }
        for (i = front; i <= rear; i++)
            printf("%d ", q[i]);
    } else {
        for (i = front; i < MAX; i++)
            printf("%d ", q[i]);
        for (i = 0; i <= rear; i++)
            printf("%d ", q[i]);
    }
    printf("\n");
}

void dequeue()
{
    int item, side;
    if (front == -1) {
        printf("\nThe Queue is empty.");
        return;
    }
    printf("\nIn which end to delete ? [1: Front 2:Rear] : ");
    scanf("%d", &side);
    if (front == rear) {
        item = q[front];
        front = rear = -1;
    } else {
        if (side == 1)
            item = q[front++];
        else if (side == 2)
            item = q[rear--];
        else
        {
            printf("\n Invalid option\n");
            return;
        }
    }
    printf("%d is deleted.\n", item);
}