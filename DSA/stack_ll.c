#include <stdio.h>
#include <stdlib.h>
typedef struct node {
  int data;
  struct node *next;
} node;

node *tos = NULL;

void push(int data) {
  node *new_node = (node *)malloc(sizeof(node));
  new_node->data = data;
  new_node->next = NULL;
  if (tos != NULL)
    new_node->next = tos;
  tos = new_node;
}

int pop(void) {
  int data;
  node *temp = tos;
  if (tos == NULL) {
    printf("\n The stack is empty !");
    return -999;
  }
  data = tos->data;
  tos = tos->next;
  free(temp);
  return data;
}
void display() {
  node *temp = tos;
  if(temp == NULL)
  {
    printf("\nStack is empty !");
    return;
  }
  while (temp != NULL) {
    printf("\n| %d |", temp->data);
    temp = temp->next;
  }
  printf("\n-----\n");
}

int main() {
  int c, data;
  do {
    printf("\n-------- STACK --------");
    printf("\n1: PUSH\t2: POP\t3: DISPLAY\t0: Exit");
    printf("\nEnter : ");
    scanf("%d", &c);
    switch (c) {
    case 1:
      printf("\nEnter the data (integer): ");
      scanf("%d", &data);
      push(data);
      break;
    case 2: data = pop();
      if(data != -999)
          printf("\n%d is popped out.",data);
      break;
    case 3: display();
      break;
    case 0: exit(0);
    }
    printf("\n");
  }while(c != 0);
  return 0;
}
