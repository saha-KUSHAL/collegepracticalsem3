#include<stdio.h>
#define MAX 100

int main(){
    int arr[MAX], size,i,j, gap;
    float count1=0,count2=0,count3=0;
    printf("Enter the size of the array: ");
    scanf("%d",&size);
    for(i=0;i<size;i++)
    {
        printf("Enter %d element: ",i+1);
        scanf("%d",&arr[i]);
    }

    printf("Before sorting the array :\n");
    for(i=0;i<size;i++)
        printf("%d\t",arr[i]);
    for(gap = size/2;gap >= 1;gap /= 2)
    {
        for(j=gap;j<size;j++){
            for(i=j-gap;i>=0;i=i-gap){
                if(arr[i + gap] > arr[i])
                    break;
                else
                {
                    int temp = arr[i+gap];
                    arr[i+gap] = arr[i];
                    arr[i] = temp;
                }
                count3++;
            }
            count2++;
        }
        count1++;
    } 

    printf("\nAfter sorting. \n");
        for(i=0;i<size;i++)
        printf("%d\t",arr[i]);
    printf("\nTotal count = %f\n count1 = %f\ncount2 = %f\ncount3 = %f\n",
    count1 + count2 + count3,count1,count2,count3);
}