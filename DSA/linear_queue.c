#include <stdio.h>
#include <stdlib.h>
typedef struct node
{
    int data;
    struct node *next;
} node;
void enqueue(int);
void display();
void dequeue();
int length();

node *front = NULL, *rear = NULL;

int main()
{
    int c, data;
    do
    {
        printf("\n-------- Linear Queue --------");
        printf("\n1: ENQUEUE\t2: DEQUEUE\t3: DISPLAY\t0: Exit");
        printf("\nEnter : ");
        scanf("%d", &c);
        switch (c)
        {
        case 1:
            printf("\nEnter the data (integer): ");
            scanf("%d", &data);
            enqueue(data);
            break;
        case 2:
            dequeue();
            break;
        case 3:
            display();
            break;
        case 0:
            exit(0);
        }
        printf("\n");
    } while (c != 0);
    return 0;
}

void enqueue(int n)
{
    node *p = (node *)malloc(sizeof(node));
    p->data = n;
    p->next = NULL;
    if (p == NULL)
    {
        printf("\nCannot allocate memory.\n");
        return;
    }
    else if (front == NULL && rear == NULL)
    {
        front = rear = p;
    }
    else
    {
        rear->next = p;
        rear = p;
    }
}

void dequeue()
{
    node *temp = front;
    if (front == NULL)
        printf("\nQueue is empty !\n");
    else
    {
        printf("\n%d is deleted.\n", front->data);
        front = front->next;
        free(temp);
    }
}

int length()
{
    node *temp = front;
    int length = 0;
    while (temp != NULL)
        length++, temp = temp->next;
    return length;
}

void display()
{
    node *temp = front;
    printf("\n");
    if (front == NULL)
    {
        printf("\nThe queue is empty.");
        return;
    }
    while (temp != NULL)
    {
        printf("%d ", temp->data);
        temp = temp->next;
    }
    printf("\n");
}